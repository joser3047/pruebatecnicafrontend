import React from 'react';
import './Sucursal.css'

const Book = ({book, onDelete, onEdit}) => {
    return (
        <tr key={book.id}>
        <td>{book.id}</td>
        <td>{book.nombre}</td>
        <td>{book.admin}</td>
        <td>{book.telefono}</td>
        <td>{book.direccion}</td>
        <td>{book.fax}</td>
        <td>{book.pedidos}</td>
        <td>{book.created_at}</td>
        <td>{book.updated_at}</td>
        <td>
            <button className="btn btn-warning"
                    onClick={() => onEdit(book)}>Edit</button>
            <button className="btn btn-danger"
                    onClick={() => onDelete(book.id)}>
            Delete</button>
        </td>
    </tr>
    );
}

export default Book;