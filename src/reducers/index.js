import { combineReducers } from 'redux';
import books from './sucursalReducer';

export default combineReducers({
    booksData: books,
});